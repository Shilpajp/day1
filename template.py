#//////////////////////////////////////////////////

#  Program Written By : Shilpa Patil
#  Subject            : python program with flask return HTML template
#  input              : passing input in the form of dictionary
#  Output             : display output as hello shilpa

#///////////////////////////////////////////////////



from flask import Flask 

app =Flask(__name__)

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Shilpa'}
    return '''
<html>
    <head>
        <title>Home Page - Microblog</title>
    </head>
    <body>
        <h1>Hello, ''' + user['username'] + '''!</h1>
    </body>
</html>'''



if __name__ == "__main__":
	app.run()